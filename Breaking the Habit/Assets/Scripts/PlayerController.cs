﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    [Flags]
    private enum Movement : byte
    {
        NONE = 0,
        LEFT = 1,
        RIGHT = 2,
        STAND = 4,
        CROUCH = 8,
        JUMP = 16,
        PUNCHLEFT = 32,
        PUNCHRIGHT = 64
    }

    [Header("COMPONENTS")] [SerializeField]
    private Camera _camera;

    [SerializeField] private Rigidbody2D
        _bodyRigidbody2D,
        _leftLegRigidbody2D,
        _rightLegRigidbody2D,
        _leftArmRigidbody2D,
        _rightArmRigidbody2D;

    [SerializeField] private Transform _leftFoot, _rightFoot, _leftHand, _rightHand;

    [Header("LEGS")] [SerializeField] private float _legRotationSpeed;
    [SerializeField] private float _leftLegSpreadAngle, _rightLegSpreadAngle;
    [SerializeField] private float _leftLegSqueezeAngle, _rightLegSqueezeAngle;

    [Header("CROUCH/STAND")] [SerializeField]
    private float _standForce;

    [SerializeField] private float _bodyRotationSpeed;
    [SerializeField] private float _crouchForce;

    [Header("JUMP")] [SerializeField] private float _jumpForce;
    [SerializeField] private float _groundedRayLength;
    [SerializeField] private LayerMask _groundedMask;

    [Header("ARMS")] [SerializeField] private float _armRotationSpeed;
    [SerializeField] private float _leftArmStandAngle, _rightArmStandAngle;
    [SerializeField] private float _leftArmPunchAngle, _rightArmPunchAngle;
    [SerializeField] private float _punchForce;
    [SerializeField] private float _punchCooldown;
    [SerializeField] private Image _leftPunchCooldownImage, _rightPunchCooldownImage;

    private Movement _movement;
    private bool _squeeze;
    private float _rightPunchTimer, _leftPunchTimer;
    private bool _init;

    private float LeftPunchTimer
    {
        get { return _leftPunchTimer; }
        set
        {
            _leftPunchTimer = value;
            if (_leftPunchTimer > _punchCooldown)
                _leftPunchTimer = _punchCooldown;
            _leftPunchCooldownImage.fillAmount = _leftPunchTimer / _punchCooldown;
        }
    }

    private float RightPunchTimer
    {
        get { return _rightPunchTimer; }
        set
        {
            _rightPunchTimer = value;
            if (_rightPunchTimer > _punchCooldown)
                _rightPunchTimer = _punchCooldown;
            _rightPunchCooldownImage.fillAmount = _rightPunchTimer / _punchCooldown;
        }
    }

    private void OnValidate()
    {
        _camera = FindObjectOfType<Camera>();
    }

    public void Init()
    {
        _init = true;
        _leftPunchTimer = _punchCooldown;
        _rightPunchTimer = _punchCooldown;
    }

    private void Update()
    {
        _movement = Movement.NONE;
        GetKey(Movement.LEFT, KeyCode.LeftArrow, KeyCode.Q);
        GetKey(Movement.RIGHT, KeyCode.RightArrow, KeyCode.D);
        GetKey(Movement.STAND, KeyCode.UpArrow, KeyCode.Z);
        GetKey(Movement.CROUCH, KeyCode.DownArrow, KeyCode.S);
        GetKey(Movement.JUMP, KeyCode.Space);

        if (Input.GetMouseButtonDown(0) && _leftPunchTimer >= _punchCooldown)
        {
            _leftPunchTimer = 0;
            _movement |= Movement.PUNCHLEFT;
        }

        if (Input.GetMouseButtonDown(1) && _rightPunchTimer >= _punchCooldown)
        {
            _rightPunchTimer = 0;
            _movement |= Movement.PUNCHRIGHT;
        }

        LeftPunchTimer += Time.deltaTime;
        RightPunchTimer += Time.deltaTime;

#if UNITY_EDITOR
        Debug.DrawRay(_leftFoot.position, Vector3.down * _groundedRayLength, Color.yellow);
        Debug.DrawRay(_rightFoot.position, Vector3.down * _groundedRayLength, Color.yellow);
        Debug.DrawRay(_leftHand.position, Vector3.down * _groundedRayLength, Color.yellow);
        Debug.DrawRay(_rightHand.position, Vector3.down * _groundedRayLength, Color.yellow);
#endif
    }

    private void FixedUpdate()
    {
        if (!_init)
            _bodyRigidbody2D.position = new Vector2(0, _bodyRigidbody2D.position.y);
        
        _squeeze = true;

        SpreadLegs(CheckMovement(Movement.LEFT), CheckMovement(Movement.RIGHT));

        if (CheckMovement(Movement.STAND))
        {
            _bodyRigidbody2D.rotation = Mathf.MoveTowardsAngle(_bodyRigidbody2D.rotation, 0, _bodyRotationSpeed * Time.deltaTime);
            if (IsGrounded() || HandsOnGround())
                _bodyRigidbody2D.velocity = Vector2.up * _standForce * Time.deltaTime;
            SqueezeLegs();
            RotateArms();
        }
        else if (CheckMovement(Movement.CROUCH))
        {
            _bodyRigidbody2D.AddForce(Vector2.down * _crouchForce * Time.deltaTime, ForceMode2D.Impulse);
            SpreadLegs();
        }

        if (CheckMovement(Movement.JUMP) && IsGrounded())
            _bodyRigidbody2D.AddForce(_bodyRigidbody2D.transform.up * _jumpForce * Time.deltaTime, ForceMode2D.Impulse);

        if (CheckMovement(Movement.PUNCHLEFT))
        {
            _leftArmRigidbody2D.rotation = Mathf.MoveTowardsAngle(_leftArmRigidbody2D.rotation, _leftArmPunchAngle,
                _armRotationSpeed * Time.deltaTime);
            _leftArmRigidbody2D.AddForce(Vector2.left * _punchForce * Time.deltaTime, ForceMode2D.Impulse);
        }
        else if (CheckMovement(Movement.PUNCHRIGHT))
        {
            _rightArmRigidbody2D.rotation = Mathf.MoveTowardsAngle(_rightArmRigidbody2D.rotation, _rightArmPunchAngle,
                _armRotationSpeed * Time.deltaTime);
            _rightArmRigidbody2D.AddForce(Vector2.right * _punchForce * Time.deltaTime, ForceMode2D.Impulse);
        }

        if (_squeeze)
            SqueezeLegs();
    }


    private void GetKey(Movement movement, params KeyCode[] keys)
    {
        foreach (var key in keys)
            if (Input.GetKey(key))
                _movement |= movement;
    }

    private bool CheckMovement(Movement movement)
    {
        return (_movement & movement) == movement;
    }

    private void SpreadLegs(bool left = true, bool right = true)
    {
        _squeeze = !left && !right;
        if (left)
            _leftLegRigidbody2D.rotation =
                Mathf.MoveTowardsAngle(_leftLegRigidbody2D.rotation, _leftLegSpreadAngle, _legRotationSpeed * Time.deltaTime);
        if (right)
            _rightLegRigidbody2D.rotation =
                Mathf.MoveTowardsAngle(_rightLegRigidbody2D.rotation, _rightLegSpreadAngle, _legRotationSpeed * Time.deltaTime);
    }

    private void SqueezeLegs()
    {
        _leftLegRigidbody2D.rotation =
            Mathf.MoveTowardsAngle(_leftLegRigidbody2D.rotation, _leftLegSqueezeAngle, _legRotationSpeed * Time.deltaTime);
        _rightLegRigidbody2D.rotation =
            Mathf.MoveTowardsAngle(_rightLegRigidbody2D.rotation, _rightLegSqueezeAngle, _legRotationSpeed * Time.deltaTime);
    }

    private void RotateArms()
    {
        _leftArmRigidbody2D.rotation =
            Mathf.MoveTowardsAngle(_leftArmRigidbody2D.rotation, _leftArmStandAngle, _armRotationSpeed * Time.deltaTime);
        _rightArmRigidbody2D.rotation =
            Mathf.MoveTowardsAngle(_rightArmRigidbody2D.rotation, _rightArmStandAngle, _armRotationSpeed * Time.deltaTime);
    }

    private bool IsGrounded()
    {
        return Physics2D.Raycast(_leftFoot.position, Vector2.down, _groundedRayLength, _groundedMask) ||
               Physics2D.Raycast(_rightFoot.position, Vector2.down, _groundedRayLength, _groundedMask);
    }

    private bool HandsOnGround()
    {
        return Physics2D.Raycast(_leftHand.position, Vector2.down, _groundedRayLength, _groundedMask) ||
               Physics2D.Raycast(_rightHand.position, Vector2.down, _groundedRayLength, _groundedMask);
    }
}