﻿using UnityEngine;

public class BottleController : MonoBehaviour
{
    [SerializeField] private ScoreController _scoreController;
    [SerializeField] private SpriteRenderer _spriteRenderer;
    [SerializeField] private Color32 _intactColor, _brokenColor;
    [SerializeField] private float _hpMax;

    private float _hp;

    private float HP
    {
        get { return _hp; }
        set
        {
            _hp = value;
            if (_hp > _hpMax)
                _hp = _hpMax;
            if (_hp <= 0)
            {
                gameObject.SetActive(false);
                _scoreController.Score++;
            }
            _spriteRenderer.color = Color32.Lerp(_brokenColor, _intactColor, _hp / _hpMax);
        }
    }

    private void OnValidate()
    {
        _scoreController = FindObjectOfType<ScoreController>();
        _spriteRenderer = GetComponent<SpriteRenderer>();
    }

    private void Start()
    {
        HP = _hpMax;
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        HP -= other.relativeVelocity.magnitude;
    }
}