﻿using TMPro;
using UnityEngine;

public class ScoreController : MonoBehaviour
{
	[SerializeField] private TextMeshProUGUI _scoreText;
	
	private ushort _score;

	public ushort Score
	{
		get { return _score; }
		set
		{
			_score = value;
			_scoreText.text = value.ToString();
		}
	}

	public void Init()
	{
		Score = 0;
	}
}
