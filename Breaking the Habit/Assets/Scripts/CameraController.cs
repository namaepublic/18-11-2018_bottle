﻿using UnityEditor;
using UnityEngine;

[ExecuteInEditMode]
public class CameraController : MonoBehaviour
{
    [SerializeField] private Transform _playerBody;
    [SerializeField] private Vector3 _offset;
    [SerializeField] private float _smoothSpeed;

#if UNITY_EDITOR
    private void Update()
    {
        if (EditorApplication.isPlaying) return;
        transform.position = _playerBody.position + _offset;
    }
#endif

    private void FixedUpdate()
    {
        transform.position = Vector3.MoveTowards(transform.position, _playerBody.position + _offset, _smoothSpeed * Time.deltaTime); 
    }
}
