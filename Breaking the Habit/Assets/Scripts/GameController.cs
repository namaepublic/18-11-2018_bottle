﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    [SerializeField] private TimerController _timerController;
    [SerializeField] private ScoreController _scoreController;
    [SerializeField] private PlayerController _playerController;
    [SerializeField] private GameObject _homePanel;

    private bool _started;
    
    private void OnValidate()
    {
        _timerController = FindObjectOfType<TimerController>();
        _scoreController = FindObjectOfType<ScoreController>();
        _playerController = FindObjectOfType<PlayerController>();
    }

    private void Start()
    {
        _started = false;
        _homePanel.SetActive(true);
    }

    private void Update()
    {
        if (_started) return;
        if (Input.GetKeyDown(KeyCode.Space))
            StartGame();
    }

    private void StartGame()
    {
        _started = true;
        _homePanel.SetActive(false);
        _timerController.Init();
        _scoreController.Init();
        _playerController.Init();
    }

    public void GameOver()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}